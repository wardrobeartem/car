#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <Wire.h>

#define TX 1
#define RX 3
#define D0 16
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13
#define D8 15

const int LMTR = D2;
const int RMTR = D5;
const int RSEN = D7;
const int LSEN = D1;
const int BUZZ = D4;

int line = 0;

#ifndef STASSID
#define STASSID "BYOD"
#define STAPSK  "zFqzkADxyjNc6EJ4"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

int period1 = 25;
int period2 = 50;
unsigned long time_now = 0;

ESP8266WebServer server(80);

String getContentType(String filename) { // convert the file extension to the MIME type
  return "text/html";
}


// Send the file to the client
bool handleFileRead(String path) { // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";         // If a folder is requested, send the index file
  String contentType = getContentType(path);            // Get the MIME type
  if (SPIFFS.exists(path)) {                            // If the file exists
    File file = SPIFFS.open(path, "r");                 // Open it
    size_t sent = server.streamFile(file, contentType); // And send it to the client
    file.close();                                       // Then close the file again
    return true;
  }
  Serial.println("\tFile Not Found");
  return false;                                         // If the file doesn't exist, return false
}

void lMotorOn() {
  digitalWrite(LMTR, 0);
}
void lMotorOff() {
  digitalWrite(LMTR, 1);
}
void rMotorOn() {
  digitalWrite(RMTR, 0);
}
void rMotorOff() {
  digitalWrite(RMTR, 1);
}
void aMotorOn() {
  digitalWrite(LMTR, 0);
  digitalWrite(RMTR, 0);
}
void aMotorOff() {
  rMotorOff();
  lMotorOff();
}
void lMotorSpeed(int speed) {
  analogWrite(LMTR, map(speed, 0, 100, 255, 0));
}
void rMotorSpeed(int speed) {
  analogWrite(RMTR, map(speed, 0, 100, 255, 0));
}
//f prefix - fast
//s prefix - slow
void sForward() {
  lMotorSpeed(85);
  rMotorSpeed(85);
}
void fLeft() {
  lMotorOff();
  rMotorOn();
}
void sLeft() {
  lMotorOff();
  rMotorSpeed(85);
}
void fForwardLeft() {
  lMotorSpeed(85);
  rMotorOn();
}
void sForwardLeft() {
  lMotorSpeed(70);
  rMotorSpeed(85);
}
void fRight() {
  lMotorOn();
  rMotorOff();
}
void sRight() {
  lMotorSpeed(85);
  rMotorOff();
}
void fForwardRight() {
  lMotorOn();
  rMotorSpeed(85);
}
void sForwardRight() {
  lMotorSpeed(85);
  rMotorSpeed(70);
}
void lineOn() {
  line = 1;
}
void lineOff(){
  aMotorOff();
  line = 0;
}
void followPath() {
  if(digitalRead(LSEN) == 1) {
    lMotorOff();
    rMotorOn();
  } else if(digitalRead(RSEN) == 1) {
    rMotorOff();
    lMotorOn();
  } else {
    aMotorOn();
  }
}

void WifiConnect() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void buzz() {

  time_now = millis();
  
   
    while(millis() < time_now + period1){
          tone(BUZZ, 1046.50 ); //  C 523.25  739.99
          
   
        //wait approx. [period] ms
    }



   
    while(millis() < time_now + period2){
        tone(BUZZ, 1479.98);// F#
        //wait approx. [period] ms
    }
}

void setup() {
  // Initialise the pins on the microcontroller
  pinMode(LMTR, OUTPUT);
  pinMode(RMTR, OUTPUT);
  pinMode(LSEN, INPUT);
  pinMode(RSEN, INPUT);
  pinMode(BUZZ, OUTPUT);

  // Start the serial interface
  Serial.begin(115200);

  aMotorOff();

  WifiConnect();


  // Start the file server
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  
  // Web Pages definitions
  server.onNotFound([]() {                              // If the client requests any URI
    if (!handleFileRead(server.uri()))                  // send it if it exists
      server.send(404, "text/plain", "This content is not available in this country");          // otherwise, respond with a 404 (Not Found) error
  });

  // Web Hooks definitions
  server.addHook([](const String&, const String & url, WiFiClient*, ESP8266WebServer::ContentTypeFunction) {
    if (url.startsWith("/fForward")) { aMotorOn(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/sForward")) { sForward(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/stop")) { aMotorOff(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/fLeft")) { fLeft(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/sLeft")) { sLeft(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/fForwardLeft")) { fForwardLeft(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/sForwardLeft")) { sForwardLeft(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/fRight")) { fRight(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/sRight")) { sRight(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/fForwardRight")) { fForwardRight(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/sForwardRight")) { sForwardRight(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/lineOn")) { lineOn(); server.send(200, "text/plain", "OK"); }
    if (url.startsWith("/lineOff")) { lineOff(); server.send(200, "text/plain", "OK"); }
    return ESP8266WebServer::CLIENT_REQUEST_CAN_CONTINUE;
  });

  // Start the web server
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  if (line == 1) {
    followPath();
  }
  server.handleClient();
  //buzz();
}
